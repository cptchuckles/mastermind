#!/bin/env ruby

class Piece
  @@valid_pieces = "RGBWKY"
  
  def self.random count=1
    result = ""
    count.times do
      result << @@valid_pieces[(rand * (@@valid_pieces.length - 1)).to_i]
    end
    result
  end
  
  def self.valid_pieces
    @@valid_pieces
  end
end

class Game
  attr_accessor :guess, :points
  attr_reader :guesses, :slots
  
  def initialize _guesses=12, _slots=4
    @guesses = _guesses
    @slots = _slots
    @guess = 0
    @points = 0
  end
end

class GuessResult
  attr_reader :bulls, :cows
  def initialize b=0, c=0
    @bulls = b
    @cows = c
  end
end


def guess game=nil
  ans = []
  validchars = Piece.valid_pieces.chars
  while ans.length < 1
    print "Enter your guess (#{validchars.join ','}): "
    ans = gets.chomp.upcase.scan Regexp.union validchars
  end

  game.guess += 1 unless game.nil?

  ans.join[0..(game.slots - 1)].ljust game.slots, '0'
end


def validate _key, _ans
  # Iterate over the key; for each char, check answer for bulls and cows
  # Remove matches to avoid dups, replace with placeholders to keep indices
  # Do bulls first
  key = _key.dup
  ans = _ans.dup
  key.chars.each_index do |k|
    if key[k] == ans[k]
      ans[k] = "b"
      key[k] = "x"
    end
  end
  key.chars.each_index do |k|
    ans.chars.each_index do |a|
      if key[k] == ans[a]
        ans[a] = "c"
        key[k] = "x"
      end
    end
  end
  bulls = ans.count 'b'
  cows = ans.count 'c'
  GuessResult.new bulls, cows
end


def get_clamped_int msg, max, min=0
  input = nil
  while input.nil?
    print "#{msg} (#{min}-#{max}): "
    input = gets.chomp.match /\d+/
  end
  n = input[0].to_i
  return min if n < min
  return max if max < n
  n
end




puts "Mastermind!\n\n"
play = true

while play
  turns = get_clamped_int "Enter number of turns", 12, 8
  puts "The game will take #{turns} turns"
  slots = get_clamped_int "Enter number of slots", 6, 4
  puts "The key will be #{slots} slots long"

  game = Game.new turns, slots
  key = Piece.random game.slots

  while game.guess < game.guesses
    g = guess game
    puts "You guessed #{g}"
    r = validate key, g
    puts "Bulls: #{r.bulls}; Cows: #{r.cows}"
    if r.bulls == game.slots
      puts "You win!"
      break
    end
  end

  puts "The key was #{key}"
  print "Do you want to play again? (y/N): "
  r = gets.chomp.downcase.match 'y'
  play = !r.nil?
end
